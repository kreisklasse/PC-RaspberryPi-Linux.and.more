# How To switch off the LED of a Pi

Sources:  
[https://kofler.info/on-board-leds-des-raspberry-pi-steuern/](https://kofler.info/on-board-leds-des-raspberry-pi-steuern/)  
[https://www.jeffgeerling.com/blogs/jeff-geerling/controlling-pwr-act-leds-raspberry-pi](https://www.jeffgeerling.com/blogs/jeff-geerling/controlling-pwr-act-leds-raspberry-pi)  
[https://raspberrypi.stackexchange.com/questions/70593/turning-off-leds-on-raspberry-pi-3](https://raspberrypi.stackexchange.com/questions/70593/turning-off-leds-on-raspberry-pi-3)  


## HowTo
Red LED = led0  
Green LED = led1  
1. Overwrite standard settings  
````sudo sh -c 'echo none > /sys/class/leds/led0/trigger'````  
````sudo sh -c 'echo none > /sys/class/leds/led1/trigger'````  


2. switching off Red powerd LED (on PI2 & 3)  
````sudo sh -c 'echo 0 > /sys/class/leds/led1/brightness'````  
