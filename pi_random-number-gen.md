# HowTo install/activate the Random Number Generator on Pi1/2/3

Source: https://www.nico-maas.de/?p=1562


***


`sudo apt-get install -y  rng-tools`  

`sudo nano /etc/default/rng-tools`  
delete # in front of  
`HRNGDEVICE=/dev/hwrng`  


`sudo service rng-tools restart`