### Add-Ons I use
* uBlock origin [Chrome](https://chrome.google.com/webstore/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm) | [Firefox](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/)
* https Everywhere [Chrome](https://chrome.google.com/webstore/detail/https-everywhere/gcbommkclmclpchllfjekcdonpmejbdp) | [Firefox](https://addons.mozilla.org/en-US/firefox/addon/https-everywhere/)
* Decentraleyes [Chrome](https://chrome.google.com/webstore/detail/decentraleyes/ldpochfccmkkmhdbclfhpagapcfdljkj) | [Firefox](https://addons.mozilla.org/en-US/firefox/addon/decentraleyes/)
* SmartReferer [Firefox](https://addons.mozilla.org/en-US/firefox/addon/smart-referer/)
* Firefox Multi-Account Containers [Firefox](https://addons.mozilla.org/en-GB/firefox/addon/multi-account-containers/)
* First Party Isolation [Firefox](https://addons.mozilla.org/en-GB/firefox/addon/first-party-isolation/)
* Temporary Containers [Firefox](https://addons.mozilla.org/en-GB/firefox/addon/temporary-containers/)