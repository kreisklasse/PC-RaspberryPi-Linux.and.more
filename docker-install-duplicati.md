
1. sudo apt-get install mono-devel
2. docker pull linuxserver/duplicati
3. docker create --name=duplicati -p 8200:8200 linuxserver/duplicati
4. docker start duplicati
5. docker update --restart=always duplicati


Sources:  
https://hub.docker.com/r/linuxserver/duplicati  
https://github.com/duplicati/duplicati/releases


### Install Ohne Docker

1. apt-get install mono-devel
2. wget https://github.com/duplicati/duplicati/releases/download/v2.0.4.19-2.0.4.19_canary_2019-06-17/duplicati_2.0.4.19-1_all.deb
3. dpkg -i duplicati_2.0.4.19-1_all.deb
4. duplicati-server --webservice-port=8200 --webservice-interface=any
5. systemctl start duplicati.service
6. systemctl enable duplicati.service


Sources:  
https://forum.duplicati.com/t/installing-duplicati-on-linux-ubuntu-linuxlite/743  
https://www.howtoforge.com/tutorial/personal-backups-with-duplicati-on-linux/  
https://forum.duplicati.com/t/install-duplicati-on-raspi-raspberry-pi/5309/3
