# Install Proxmox on an Laptop x86-64bit

### Proxmox VE Packages Repos
https://pve.proxmox.com/wiki/Package_Repositories
https://pve.proxmox.com/pve-docs/pve-admin-guide.html#sysadmin_package_repositories

add
```
nano /etc/apt/sources.list
```
```
# PVE pve-no-subscription repository provided by proxmox.com,
# NOT recommended for production use
deb http://download.proxmox.com/debian/pve buster pve-no-subscription
```

disable with # at (due to no subsccrption witch proxmox)
```
nano /etc/apt/sources.list.d/pve-enterprise.list
```
```
deb https://enterprise.proxmox.com/debian/pve buster pve-enterprise
```

### Time Sync
https://pve.proxmox.com/pve-docs/pve-admin-guide.html#_time_synchronization

```
nano /etc/systemd/timesyncd.conf
```